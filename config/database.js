module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: env('DATABASE_HOST', '45.141.77.128'),
        port: env.int('DATABASE_PORT', 5432),
        database: env('DATABASE_NAME', 'vegastyle'),
        username: env('DATABASE_USERNAME', 'doodlez79'),
        password: env('DATABASE_PASSWORD', 'password'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
