module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  url: "https://vegastyle58.ru/api",
  admin: {
    url: 'https://vegastyle58.ru/dashboard',
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'a1bb707c1d7899dd00e6b1b7d35803df'),
    },
  },
});
